s_1, s_2, s_3 = input(), input(), input()
s_max = max(s_1, s_2, s_3)
s_min = min(s_1, s_2, s_3)
if s_1 != s_max and s_1 != s_min:
    print(s_max + ", " + s_1 + ", " + s_min + ".")
if s_2 != s_max and s_2 != s_min:
    print(s_max + ", " + s_2 + ", " + s_min + ".")
if s_3 != s_max and s_3 != s_min:
    print(s_max + ", " + s_3 + ", " + s_min + ".")