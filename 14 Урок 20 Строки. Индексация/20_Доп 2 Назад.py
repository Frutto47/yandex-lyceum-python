alphabet_upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
alphabet_lower = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.lower()
word = input()
new_word = ''
for char in word:
    if 65 <= ord(char) <= 65 + 26:  # если буква большая
        if ord(char) - 3 < 65:
            new_word += chr(ord(char) + 26 - 3)
        else:
            new_word += chr(ord(char) - 3)
    else:
        if ord(char) - 3 < 97:
            new_word += chr(ord(char) + 26 - 3)
        else:
            new_word += chr(ord(char) - 3)
print(new_word[0] + new_word[len(word) // 2] + new_word[-2] + new_word[-1])