# -*- coding: utf-8 -*-
n = int(input())
name_1, name_2 = None, None
basket_1, basket_2 = set(), set()
for i in range(n):
    name = input()
    basket = set()
    mushroom = input()
    while mushroom != 'ВСЕ':
        basket.add(mushroom)
        mushroom = input()
    if name_1 is None:
        name_1 = name
        basket_1 = basket
    elif name_2 is None:
        name_2 = name
        basket_2 = basket
    else:
        if len(basket) > len(basket_1):
            if len(basket_1) > len(basket_2) or \
                    len(basket_1) == len(basket_2) and name_1 < name_2:
                name_2, basket_2 = name_1, basket_1
            name_1 = name
            basket_1 = basket
        elif len(basket) == len(basket_1) and name < name_1:
            if len(basket_1) > len(basket_2) or \
                    len(basket_1) == len(basket_2) and name_1 < name_2:
                name_2, basket_2 = name_1, basket_1
            name_1 = name
            basket_1 = basket
        elif len(basket) > len(basket_2):
            name_2 = name
            basket_2 = basket
        elif len(basket) == len(basket_2) and name < name_2:
            name_2 = name
            basket_2 = basket
if len(basket_2) > len(basket_1):
    name_1, name_2 = name_2, name_1
if name_1 > name_2 and len(basket_1) == len(basket_2):
    name_1, name_2 = name_2, name_1
print(name_1, name_2, sep='\n')
print('РАЗНЫЕ' if basket_1 != basket_2 else 'ОДИНАКОВЫЕ')