symbol = input()
answer = set()
max_length = 0
for i in range(int(input())):
    word = set(input())
    word.discard(symbol)
    if len(word) > max_length:
        max_length = len(word)
        answer.clear()
        answer = word
if max_length == 0:
    print(-1)
else:
    print(*answer, sep='')
