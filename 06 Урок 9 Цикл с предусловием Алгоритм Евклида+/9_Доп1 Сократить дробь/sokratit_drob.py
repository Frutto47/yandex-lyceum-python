nom_1 = int(input())
denom_1 = int(input())
nom_2 = int(input())
denom_2 = int(input())
denom_to_increase = 0
if denom_1 > denom_2:
    denom_max = denom_1
    denom_to_increase = 2
elif denom_2 > denom_1:
    denom_max = denom_2
    denom_to_increase = 1
else:
    denom_max = denom_1
while min(denom_1, denom_2) != denom_max:
    if denom_to_increase == 1:
        denom_1 += denom_1
        nom_1 += nom_1
    elif denom_to_increase == 2:
        denom_2 += denom_2
        nom_2 += nom_2
    else:
        break
while nom_1 - nom_2 > 0 and
print(nom_1 - nom_2, '/', denom_max, sep='')
