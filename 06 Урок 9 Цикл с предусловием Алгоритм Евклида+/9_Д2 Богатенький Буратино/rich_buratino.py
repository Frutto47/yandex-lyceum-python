cost = int(input())
gold = 1
cycle = 0
while gold < cost:
    gold *= 10
    cycle += 1
print(cycle)