div_needed = int(input())
div_count = 0
cur_div = 1
found = False
cur_number = 1

while not found:
    while cur_div <= cur_number:
        if cur_number % cur_div == 0:
            div_count += 1
        cur_div += 1
    if div_count == div_needed:
        found = True
        print(cur_number)
    cur_div = 1
    div_count = 0
    cur_number += 1