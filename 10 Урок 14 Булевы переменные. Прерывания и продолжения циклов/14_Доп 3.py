n = int(input())
count = 1
for i in range(2, int(n ** 0.5) + 1):
    if not n % i:
        while not n % i:
            n //= i
        count *= i
        if n == 1:
            break
if n != 1:
    count *= n
print(count)