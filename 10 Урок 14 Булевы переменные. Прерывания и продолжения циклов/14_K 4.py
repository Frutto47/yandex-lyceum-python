s, kind, angry = input(), 0, 0
while s != '':
    if s == 'добрый':
        kind += 1
    if s == 'злой':
        angry += 1
    if s == 'Какой подарок?':
        if kind > angry and prev_s == 'добрый':
            print('серебряный шиллинг')
            kind = angry = 0
        elif kind < angry and prev_s == 'злой':
            print('золотой')
            kind = angry = 0
        else:
            print('Ах, не знаю!')
            break
    prev_s = s
    s = input()