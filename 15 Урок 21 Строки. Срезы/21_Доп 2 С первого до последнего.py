symbol = input()
line = input()

first_index = last_index = index = 0
first_index_found = False

for i in line:
    if i == symbol:
        first_index_found = True
        first_index = index
        break
    index += 1
index = len(line) - 1
for i in line[::-1]:
    # print(i, end='')
    if i == symbol:
        last_index = index
        break
    index -= 1
# print(first_index)
# print(last_index)
if not first_index_found:
    print(line)
elif first_index == last_index:
    print(line[first_index + 1:])
else:
    print(line[first_index + 1:last_index])
