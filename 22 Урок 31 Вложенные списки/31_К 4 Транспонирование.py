rows = int(input())
cols = int(input())
matrix = [[0] * cols for _ in range(rows)]
t_matrix = [[0] * rows for _ in range(cols)]

for row in range(rows):
    for col in range(cols):
        matrix[row][col] = input()

for col in range(cols):
    for row in range(rows):
        t_matrix[col][row] = matrix[row][col]

for row in range(rows):
    print('; '.join(matrix[row]))

print()

for col in range(cols):
    print('; '.join(t_matrix[col]))
