side = int(input())
matrix = list()
answer = list()
for i in range(side):
    matrix.append(input().split())
#  print(*matrix, sep='\n')

for i in range(1, side - 1):
    for j in range(1, len(matrix[0]) - 1):
        answer.append(matrix[i][j] + matrix[i - 1][j - 1] + matrix[i - 1][j + 1] + matrix[i + 1][j - 1] + matrix[i + 1][
            j + 1])
        #  print(answer)
        #  print(matrix[i][j], end=' ')
    #  print()

for i in range(0, len(answer), len(matrix[0]) - 2):
    print(' '.join(answer[i:i + len(matrix[0]) - 2]))
