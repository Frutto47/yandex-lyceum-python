side = int(input())
matrix = list()
summ = 0
for i in range(side):
    matrix.append(input().split(', '))

for i in range(side):
    matrix[i][i], matrix[i][side - 1 - i]\
        = matrix[i][side - 1 - i],  matrix[i][i]
    summ += int(matrix[i][i]) + int(matrix[i][side - 1 - i])
    for j in range(side):
        print(matrix[i][j], end=' ')
    print()
print(summ)
