line = input().split()
word_list = list()
# print(line)
# Process finished with exit code 0
for i in range(len(line)):
    if len(line[i]) % 2 == 0:
        x = len(line[i]) // 2
        y = len(line[i])
        print(f'слово \'{line[i]}\' чётное, сверху 2 буквы, массив размером {x} x {y}')
        word_list = [[0] * x for _ in range(y)]
        # print('До')
        # print(*word_list, sep='\n')
        for row in range(x):
            if row == 0:
                word_list[row][len(word_list) // 2 - 1] = line[i][len(line[i]) // 2 - 1:len(line[i]) // 2]
            else:
                pass
                # word_list[row][len(word_list) // 2 - 1 - row] = line[i][len(line[i]) // 2 - 1 - row:len(line[i]) // 2]
                # word_list[row][len(word_list) // 2 - 1 + row] = line[i][len(line[i]) // 2 - 1 + row:len(line[i]) // 2]
        print('После')
        print(*word_list, sep='\n')
    else:
        print(f'слово \'{line[i]}\' нечётное, сверху 1 буква, массив размером {len(line[i]) // 2 + 1} x {len(line[i])}')
        word_list = [[0] * len(line[i]) for _ in range(len(line[i]) // 2 + 1)]
        print('После')
        print(*word_list, sep='\n')
print(')')