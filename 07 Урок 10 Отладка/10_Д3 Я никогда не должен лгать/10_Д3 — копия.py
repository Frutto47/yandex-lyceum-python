last_line = line = input()
while line:
    if len(str(len(line))) == 3:
        print(last_line)
    elif len(line) % 2 == 0:
        print(line * 2)
    elif len(line) % 3 == 0:
        print(line * 3)
    else:
        print(line)
    last_line = line
    line = input()