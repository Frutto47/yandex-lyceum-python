part = int(input())
mercury = part * 2
sulfur = part / 3
ammonia = part / 8
mass = mercury + sulfur + ammonia
half_mass = mass / 2
hours_count = 0

while mass > half_mass:
    mercury = mercury - mercury / 100
    ammonia = ammonia - ammonia / 200
    mass = mercury + sulfur + ammonia
    hours_count += 1
print(hours_count, mass)
