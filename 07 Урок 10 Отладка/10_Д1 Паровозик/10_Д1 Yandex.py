hours, minutes, number_of_stops = int(input()), int(input()), int(input())
duration = (12 * 60 - hours * 60 - minutes) / number_of_stops
print(f'{int(duration)} минут' if duration > 0 else 'Не останавливаемся!')