k, b  = int(input()), int(input())
x, y = int(input()), int(input())
above_count = below_count = belong_count = 0

def f(x, k=k, b=b): # y = k * x + b
    return k * x + b

while True:
    x = int(x)
    y = int(y)
    if f(x) < y:
        above_count += 1
    if f(x) > y:
        below_count += 1
    if f(x) == y:
        belong_count += 1
        
    x = input()
    if x == 'END':
        break
    y = input()
    if y == 'END':
        break
    
if above_count > 0:
    print('Выше прямой:', above_count)
if below_count > 0:
    print('Ниже прямой:', below_count)
if belong_count > 0:
    print('На прямой:', belong_count)