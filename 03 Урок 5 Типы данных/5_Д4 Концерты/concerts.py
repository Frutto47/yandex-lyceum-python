g1, g2, money = input(), input(), int(input())
a = 'A'
b = 'B'
no = 'NOT TO GO'
if g1 == a:
    if g2 == a or g2 == b:
        print(no)
    else:
        print((money - 5) // 2)
elif g1 == b:
    if g2 == a or g2 == b:
        print(no)
    else:
        print((money - 3) // 2)
elif g2 == a:
    print((money - 5) // 2)
elif g2 == b:
    print((money - 3) // 2)
else:
    print(no)
