components = []
component = input()
while component:
    component = component.split()
    components.append(component)
    component = input()

line = input()
while line:
    for i in range(len(components)):
        if line in components[i]:
            components[i].remove(line)
    line = input()

for i in range(len(components)):
    if len(components[i]) == 1:
        print(components[i][0])
