s_1 = input()
s_2 = input()
s_3 = input()

r_1 = min(len(s_1), len(s_2), len(s_3))
r_3 = max(len(s_1), len(s_2), len(s_3))
str_min = str_mid = str_max = ''
if len(s_1) == len(s_2) == len(s_3) == r_1:
    if s_1 > s_2:
        s_1, s_2 = s_2, s_1
    if s_1 > s_3:
        s_1, s_3 = s_3, s_1
    if s_2 > s_3:
        s_2, s_3 = s_3, s_2
    str_min, str_mid, str_max = s_1, s_2, s_3
elif len(s_1) == len(s_2) == r_1:
    if s_2 < s_1:
        s_1, s_2 = s_2, s_1
    str_min, str_mid, str_max = s_1, s_2, s_3
elif len(s_3) == len(s_2) == r_1:
    if s_3 < s_2:
        s_3, s_2 = s_2, s_3
    str_min, str_mid, str_max = s_2, s_3, s_1
elif len(s_3) == len(s_1) == r_1:
    if s_3 < s_1:
        s_3, s_1 = s_1, s_3
    str_min, str_mid, str_max = s_1, s_3, s_2
elif len(s_1) == r_1:
    if len(s_3) == len(s_2) == r_3:
        if s_3 < s_2:
            s_3, s_2 = s_2, s_3
    elif len(s_2) == r_3:
        s_3, s_2 = s_2, s_3
    str_min, str_mid, str_max = s_1, s_2, s_3
elif len(s_2) == r_1:
    if len(s_1) == len(s_3) == r_3:
        if s_3 < s_1:
            s_3, s_1 = s_1, s_3
    elif len(s_1) == r_3:
        s_3, s_1 = s_1, s_3
    str_min, str_mid, str_max = s_2, s_1, s_3
elif len(s_3) == r_1:
    if len(s_1) == len(s_2) == r_3:
        if s_2 < s_1:
            s_1, s_2 = s_2, s_1
    elif len(s_1) == r_3:
        s_1, s_2 = s_2, s_1
    str_min, str_mid, str_max = s_3, s_1, s_2

print(str_min)
print(str_mid)
print(str_max)