number = int(input())
n1 = number // 1000
n2 = number % 1000 // 100
n3 = number % 100 // 10
n4 = number % 10
nm4 = max(n1, n2, n3, n4)
nm1 = min(n1, n2, n3, n4)
nm2 = nm3 = ''
if n1 != nm1 and n1 != nm4 and not nm2:
    nm2 = n1
elif n2 != nm1 and n2 != nm4 and not nm2:
    nm2 = n2
elif n3 != nm1 and n3 != nm4 and not nm2:
    nm2 = n3
elif n4 != nm1 and n4 != nm4 and not nm2:
    nm2 = n4
if n1 != nm1 and n1 != nm4 and n1 != nm2 and not nm3:
    nm3 = n1
elif n2 != nm1 and n2 != nm4 and n2 != nm2 and not nm3:
    nm3 = n2
elif n3 != nm1 and n3 != nm4 and n3 != nm2 and not nm3:
    nm3 = n3
elif n4 != nm1 and n4 != nm4 and n4 != nm2 and not nm3:
    nm3 = n4
if nm2 > nm3:
    nm2, nm3 = nm3, nm2
if nm1 == 0:
    nm1, nm2 = nm2, nm1
print(nm1 * 10 + nm2, nm4 * 10 + nm3)