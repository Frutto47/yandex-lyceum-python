number = int(input())
seven_count = 0
while number > 0:
    while number > 0:
        if number % 8 == 7:
            seven_count += 1
        number //= 8
    number = int(input())
print(seven_count)