n = int(input())
for x in range(1, n + 1):
    for y in range(x + 1, n + 1):
        z = int((x ** 2 + y ** 2) ** 0.5)
        if z <= n and z ** 2 == x ** 2 + y ** 2:
            for i in range(2, x):
                if x % i == 0 and y % i == 0 and z % i == 0:
                    break
            else:
                print(x, y, z)